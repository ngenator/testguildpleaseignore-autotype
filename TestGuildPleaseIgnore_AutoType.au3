#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Icon=..\~Icons\1_Desktop_Icons\icon_512.ico
#AutoIt3Wrapper_Outfile=TestGuildPleaseIgnore_AutoType.exe
#AutoIt3Wrapper_Res_requestedExecutionLevel=asInvoker
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
; Test Guild Please Ignore
; Announcement App

Global $Ready = False
Global $iMessageMax = 190

Global Enum $INFO, $NOTICE, $ALERT
Global Enum $SAY, $MAP, $TEAM, $SQUAD, $GUILD, $PARTY

Opt("SendKeyDelay",1)
Opt("SendKeyDownDelay",1)

HotKeySet("{HOME}","LoadMessages")
HotKeySet("{END}","Quit")

Global $sIniFile 		= "Messages.ini"
Global $stAnnouncement 	= "~!~ %s ~!~"
Global $fUsePasteMethod = True

Global $sSectionOne 	= "RecruitingInfo", 	$sSectionOneChannel, 	$asSectionOne, 		$iSectionOneCount, 		$iSectionOneIndex 	= 1
Global $sSectionTwo 	= "ServerInfo", 		$sSectionTwoChannel, 	$asSectionTwo, 		$iSectionTwoCount, 		$iSectionTwoIndex 	= 1
Global $sSectionThree 	= "GuildInfo", 			$sSectionThreeChannel, 	$asSectionThree, 	$iSectionThreeCount, 	$iSectionThreeIndex = 1
Global $sSectionFour 	= "CommunicationInfo", 	$sSectionFourChannel, 	$asSectionFour, 	$iSectionFourCount, 	$iSectionFourIndex 	= 1

ToolTip(StringFormat("Created by Ngenator for Test Guild Please Ignore\n\nAbout\n=====\n  Reads sections from a .ini file and types them when a hotkey is pressed.\n\nHotkeys\n=====\n  Exit: {END}\n  Reload Messages: {HOME}\n  Message hotkeys are set in Messages.ini\n\nPRESS ENTER TO CONTINUE..."), 0, 0, "Auto Typer")
HotKeySet("{ENTER}","Ready")

While Not $Ready
	Sleep(100)
WEnd

If Not LoadMessages() Then Exit
LoadSettings()

While True
	Sleep(100)
WEnd

Func Ready()
	HotKeySet("{ENTER}")
	ToolTip("")
	$Ready = True
EndFunc

Func LoadSettings()
	HotKeySet(IniRead($sIniFile, "Hotkeys", $sSectionOne, 	"{F5}"), "One")
	HotKeySet(IniRead($sIniFile, "Hotkeys", $sSectionTwo, 	"{F6}"), "Two")
	HotKeySet(IniRead($sIniFile, "Hotkeys", $sSectionThree, "{F7}"), "Three")
	HotKeySet(IniRead($sIniFile, "Hotkeys", $sSectionFour, 	"{F8}"), "Four")

	$sSectionOneChannel 	= IniRead($sIniFile, "Channels", $sSectionOne,	 "None")
	$sSectionTwoChannel 	= IniRead($sIniFile, "Channels", $sSectionTwo,	 "None")
	$sSectionThreeChannel 	= IniRead($sIniFile, "Channels", $sSectionThree, "None")
	$sSectionFourChannel 	= IniRead($sIniFile, "Channels", $sSectionFour,	 "None")
EndFunc

Func LoadMessages()
	If Not FileExists($sIniFile) Then
		MsgBox(16, "Error", "File not found: " & $sIniFile)
		IniWriteSection($sIniFile, "Hotkeys", $sSectionOne & "={F5}" & @LF & $sSectionTwo & "={F6}" & @LF & $sSectionThree & "={F7}" & @LF & $sSectionFour & "={F8}")
		IniWriteSection($sIniFile, "Channels", $sSectionOne & "=" & @LF & $sSectionTwo & "=" & @LF & $sSectionThree & "=" & @LF & $sSectionFour & "=")
		IniWriteSection($sIniFile, $sSectionOne, 	"=Message 1" & @LF & "=Message 2" & @LF & "=Etc...")
		IniWriteSection($sIniFile, $sSectionTwo, 	"=Message 1" & @LF & "=Message 2" & @LF & "=Etc...")
		IniWriteSection($sIniFile, $sSectionThree, 	"=Message 1" & @LF & "=Message 2" & @LF & "=Etc...")
		IniWriteSection($sIniFile, $sSectionFour, 	"=Message 1" & @LF & "=Message 2" & @LF & "=Etc...")
		Return False
	EndIf

	$asSectionOne = IniReadSection($sIniFile, $sSectionOne)
	If @Error Then
		MsgBox(16, "Error", $sIniFile & " is formatted incorrectly. Section [" & $sSectionOne & "] is missing or empty.")
		Return False
	EndIf
	$iSectionOneCount = $asSectionOne[0][0]

	$asSectionTwo = IniReadSection($sIniFile, $sSectionTwo)
	If @Error Then
		MsgBox(16, "Error", $sIniFile & " is formatted incorrectly. Section [" & $sSectionTwo & "] is missing or empty.")
		Return False
	EndIf
	$iSectionTwoCount = $asSectionTwo[0][0]

	$asSectionThree = IniReadSection($sIniFile, $sSectionThree)
	If @Error Then
		MsgBox(16, "Error", $sIniFile & " is formatted incorrectly. Section [" & $sSectionThree & "] is missing or empty.")
		Return False
	EndIf
	$iSectionThreeCount = $asSectionThree[0][0]

	$asSectionFour = IniReadSection($sIniFile, $sSectionFour)
	If @Error Then
		MsgBox(16, "Error", $sIniFile & " is formatted incorrectly. Section [" & $sSectionFour & "] is missing or empty.")
		Return False
	EndIf
	$iSectionFourCount = $asSectionFour[0][0]

	SetTooltip("Info", "Loaded messages from " & $sIniFile)

	Return True
EndFunc

Func One()
	If $iSectionOneIndex > $iSectionOneCount Then $iSectionOneIndex = 1
	If $asSectionOne[$iSectionOneIndex][1] <> "" Then
		SetTooltip($sSectionOne, "Sending message " & $iSectionOneIndex)
		If Not ($sSectionOneChannel == "None" Or $sSectionOneChannel == "") Then
			__Send("/" & $sSectionOneChannel)
			Sleep(100)
		EndIf
		__Send($asSectionOne[$iSectionOneIndex][1], False)
	EndIf
	$iSectionOneIndex += 1
EndFunc

Func Two()
	If $iSectionTwoIndex > $iSectionTwoCount Then $iSectionTwoIndex = 1
	If $asSectionTwo[$iSectionTwoIndex][1] <> "" Then
		SetTooltip($sSectionTwo, "Sending message " & $iSectionTwoIndex)
		If Not ($sSectionTwoChannel == "None" Or $sSectionTwoChannel == "") Then
			__Send("/" & $sSectionTwoChannel)
			Sleep(100)
		EndIf
		__Send($asSectionTwo[$iSectionTwoIndex][1], False)
	EndIf
	$iSectionTwoIndex += 1
EndFunc

Func Three()
	If $iSectionThreeIndex > $iSectionThreeCount Then $iSectionThreeIndex = 1
	If $asSectionThree[$iSectionThreeIndex][1] <> "" Then
		SetTooltip($sSectionThree, "Sending message " & $iSectionThreeIndex)
		If Not ($sSectionThreeChannel == "None" Or $sSectionThreeChannel == "") Then
			__Send("/" & $sSectionThreeChannel)
			Sleep(100)
		EndIf
		__Send($asSectionThree[$iSectionThreeIndex][1], False)
	EndIf
	$iSectionThreeIndex += 1
EndFunc

Func Four()
	If $iSectionFourIndex > $iSectionFourCount Then $iSectionFourIndex = 1
	If $asSectionFour[$iSectionFourIndex][1] <> "" Then
		SetTooltip($sSectionFour, "Sending message " & $iSectionFourIndex)
		If Not ($sSectionFourChannel == "None" Or $sSectionFourChannel == "") Then
			__Send("/" & $sSectionFourChannel)
			Sleep(100)
		EndIf
		__Send($asSectionFour[$iSectionFourIndex][1], False)
	EndIf
	$iSectionFourIndex += 1
EndFunc

Func SetChannel($iChannel)
	Local $sChannel = ""
	Switch ($iChannel)
		Case $SAY
			$sChannel = "/say"
		Case $MAP
			$sChannel = "/map"
		Case $TEAM
			$sChannel = "/team"
		Case $SQUAD
			$sChannel = "/squad"
		Case $GUILD
			$sChannel = "/guild"
		Case $PARTY
			$sChannel = "/party"
	EndSwitch
	If $sChannel <> "" Then Send("{ENTER}" & $sChannel & "{ENTER}")
EndFunc

Func NotifyAnnouncement($sType)
	Local $sMessage = StringFormat($stAnnouncement, $sType)
	_Send($sMessage)
EndFunc

Func Announcement($iType, $sMessage)
	Switch ($iType)
		Case $INFO
			$sMessage = "[INFO] " & $sMessage
		Case $NOTICE
			$sMessage = "[NOTICE] " & $sMessage
		Case $ALERT
			$sMessage = "[ALERT] " & $sMessage
		Case Else
	EndSwitch
EndFunc

Func SetTooltip($sTitle, $sMessage, $iTimeout=2500)
	ToolTip($sMessage, 0, 0, $sTitle)
	AdlibRegister("ClearTooltip", $iTimeout)
EndFunc

Func ClearTooltip()
	ToolTip("")
	AdlibUnRegister("ClearTooltip")
EndFunc

Func _Send($sMessage, $fSendReturnBefore = True, $fSendReturnAfter = True, $fAntiBlock = False)
	If StringLen($sMessage) > $iMessageMax Then Return MsgBox(16,"Error","The message length must be no greater than " & $iMessageMax)
	If $fAntiBlock Then $sMessage &= " " & RandomNumber()
	ClipPut($sMessage)
	If $fSendReturnBefore Then Send("{ENTER}")
	Sleep(1000)
	Send("^v")
	Sleep(100)
	If $fSendReturnAfter Then Send("{ENTER}")
EndFunc

Func __Send($sMessage, $fSendReturnBefore = True, $fSendReturnAfter = True, $fAntiBlock = False)
	If StringLen($sMessage) > $iMessageMax Then Return MsgBox(16,"Error","The message length must be no greater than " & $iMessageMax)
	If $fAntiBlock Then $sMessage &= " " & RandomNumber()
	If $fSendReturnBefore Then
		Send("{ENTER}")
		Sleep(100)
	EndIf
	BlockInput(1)
	Send($sMessage, 1)
	BlockInput(0)
	If $fSendReturnAfter Then
		Sleep(100)
		Send("{ENTER}")
	EndIf
EndFunc

Func RandomNumber($iMin = 100, $iMax = 1000)
	Return "=" & Round(Random($iMin, $iMax)) & "="
EndFunc   ;==>RandomNumber

Func Quit()
	Exit
EndFunc
